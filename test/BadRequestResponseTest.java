import org.junit.Test;
import java.io.IOException;
import static org.junit.Assert.*;

public class BadRequestResponseTest {

    @Test
    public void unknownRequest() throws IOException {
        Response response = new BadRequestResponse();

        String statusCode = response.getStatusCode().toString();

        assertEquals(0, response.getHeaders().size());
        assertArrayEquals(("").getBytes(), response.getBody());
        assertEquals("BAD_REQUEST", statusCode);
    }
}