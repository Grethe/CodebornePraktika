import org.junit.Test;
import java.io.IOException;
import static java.util.Collections.singletonList;
import static org.junit.Assert.*;

public class HeadResponseTest {

    @Test
    public void headRequest() throws IOException {
        Request request = new Request(singletonList("HEAD /test.txt"));
        Response response = new HeadResponse(request);

        String statusCode = response.getStatusCode().toString();

        assertEquals(1, response.getHeaders().size());
        assertEquals("13", response.getHeaders().get("Content-Length").toString());
        assertArrayEquals(("").getBytes(), response.getBody());
        assertEquals("OK", statusCode);
    }

    @Test
    public void headFileNotFound() throws IOException {
        Request request = new Request(singletonList("HEAD /test.tx"));
        Response response = new HeadResponse(request);

        String statusCode = response.getStatusCode().toString();

        assertEquals(0, response.getHeaders().size());
        assertArrayEquals(("").getBytes(), response.getBody());
        assertEquals("NOT_FOUND", statusCode);
    }
}