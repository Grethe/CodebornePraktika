import org.junit.Before;
import org.junit.Test;
import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class ClientHandlerTest {
    private OutputStream out;
    private ClientHandler clientHandler;
    private Socket socket;
    private BufferedReader in;
    private Request request;

    @Before
    public void mockObjects() {

        out = mock(OutputStream.class);
        clientHandler = spy(ClientHandler.class);
        socket = spy(Socket.class);
        in = mock(BufferedReader.class);
        request = mock(Request.class);
    }

    @Test
    public void handle() throws IOException {

        List<String> requestedLines = singletonList("line");

        doReturn(requestedLines).when(clientHandler).requestLines(any());
        doReturn(new ByteArrayInputStream("".getBytes())).when(socket).getInputStream();
        doReturn(out).when(socket).getOutputStream();

        Request request = mock(Request.class);
        Response response = mock(Response.class);

        doReturn(request).when(clientHandler).bodyToRequest(same(requestedLines), any());
        doReturn(response).when(clientHandler).getResponse(request);
        doReturn(new byte[]{'x', 'y'}).when(response).responseToClient();

        clientHandler.handle(socket);

        verify(out).write(new byte[]{'x', 'y'});
        verify(out).flush();
    }

    @Test
    public void emptyMapHandle() throws IOException {

        doReturn(new ByteArrayInputStream("".getBytes())).when(socket).getInputStream();
        doReturn(out).when(socket).getOutputStream();

        clientHandler.handle(socket);

        verify(out, never()).write(any());
    }

    @Test
    public void requestedLines() throws IOException {

        doReturn("test 1").doReturn("test 2").doReturn(null).when(in).readLine();

        List<String> requestedLines = clientHandler.requestLines(in);

        assertEquals(2, requestedLines.size());
        assertEquals("test 1", requestedLines.get(0));
        assertEquals("test 2", requestedLines.get(1));
    }

    @Test
    public void nullRequestedLines() throws IOException {

        doReturn(null).when(in).readLine();

        List<String> requestedLines = clientHandler.requestLines(in);

        assertEquals(0, requestedLines.size());
    }

    @Test
    public void lineAfterNullRequestedLines() throws IOException {

        doReturn(null).doReturn("test 1").when(in).readLine();

        List<String> requestedLines = clientHandler.requestLines(in);

        assertEquals(0, requestedLines.size());
    }

    @Test
    public void getContentLength() {

        List <String> requestedLines = singletonList("Content-Length: 5");

        int contentLength = clientHandler.getContentLength(requestedLines);

        assertEquals(5, contentLength);
    }

    @Test
    public void noContentLength() {

        List <String> requestedLines = singletonList("Accept: text/plain");

        int contentLength = clientHandler.getContentLength(requestedLines);

        assertEquals(0, contentLength);
    }

    @Test
    public void noRequestHeaderContentLength() {

        List <String> requestedLines = emptyList();

        int contentLength = clientHandler.getContentLength(requestedLines);

        assertEquals(0, contentLength);
    }

    @Test
    public void requestBody() throws IOException {

        int contentLength = 5;

        doReturn(5).when(in).read(any(), anyInt(), anyInt());

        char[] body = clientHandler.requestBody(contentLength, in);

        assertEquals(5, body.length);
        verify(in).read(body, 0, 5);
        verifyNoMoreInteractions(in);
    }

    @Test
    public void contentLengthShorterRequestBody() throws IOException {

        int contentLength = 5;

        doReturn(2).doReturn(2).doReturn(2).when(in).read(any(), anyInt(), anyInt());

        char[] body = clientHandler.requestBody(contentLength, in);

        assertEquals(5, body.length);
        verify(in).read(body, 0, 5);
        verify(in).read(body, 2, 3);
        verify(in).read(body, 4, 1);
        verifyNoMoreInteractions(in);
    }

    @Test
    public void contentLengthLongerRequestBody() throws IOException {

        int contentLength = 5;

        doReturn(2).doReturn(2).doReturn(-1).when(in).read(any(), anyInt(), anyInt());

        char[] body = clientHandler.requestBody(contentLength, in);

        assertEquals(5, body.length);
        verify(in).read(body, 0, 5);
        verify(in).read(body, 2, 3);
        verify(in).read(body, 4, 1);
        verifyNoMoreInteractions(in);
    }

    @Test
    public void contentLengthZeroRequestBody() throws IOException {

        int contentLength = 0;

        char[] body = clientHandler.requestBody(contentLength, in);

        assertEquals(0, body.length);
        verify(in, never()).read(any(), anyInt(), anyInt());
    }

    @Test
    public void bodyToRequest() throws IOException {
        List<String> requestedLines = new ArrayList<>();
        requestedLines.add("GET /test.txt ");
        requestedLines.add("Content-Length: 5");

        ByteArrayInputStream testBody = new ByteArrayInputStream("abcde".getBytes());
        BufferedReader in = new BufferedReader(new InputStreamReader(testBody));

        Request request = clientHandler.bodyToRequest(requestedLines,in);

        assertArrayEquals("abcde".toCharArray(), request.requestBody);

    }

    @Test
    public void getResponse() {

        when(request.getMethod()).thenReturn("GET");
        when(request.getResource()).thenReturn("/test.tx");

        Response response = new ClientHandler().getResponse(request);

        assertTrue(response instanceof GetResponse);
    }

    @Test
    public void headResponse() {

        when(request.getMethod()).thenReturn("HEAD");
        when(request.getResource()).thenReturn("/test.txt");

        Response response = new ClientHandler().getResponse(request);

        assertTrue(response instanceof HeadResponse);
    }

    @Test
    public void badRequestResponse() {

        when(request.getMethod()).thenReturn("POST");
        when(request.getResource()).thenReturn("/test.txt");

        Response response = new ClientHandler().getResponse(request);

        assertTrue(response instanceof BadRequestResponse);
    }
}