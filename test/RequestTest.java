import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import static java.util.Collections.singletonList;
import static org.junit.Assert.*;

public class RequestTest {

    private List<String> requestedLines;

    @Before
    public void createObjects() {
        requestedLines = new ArrayList<>();
    }

    @Test
    public void getMethod() {
        Request request = new Request(singletonList("GET /test.txt HTTP/1.1"));
        String method = request.getMethod();

        assertEquals("GET", method);
    }

    @Test
    public void getRequest() {
        Request request = new Request(singletonList("GET /test.txt HTTP/1.1"));
        String fileName = request.getResource();

        assertEquals("/test.txt", fileName);
    }

    @Test
    public void getRequestHeaders() {
        requestedLines.add("GET");
        requestedLines.add("key1:value1");
        requestedLines.add("key2:value2");

        Request request = new Request(requestedLines);

        Map<String, String> requestHeaders = request.getRequestHeaders();

        assertEquals(2, requestHeaders.size());
        assertEquals("value1", requestHeaders.get("key1"));
        assertEquals("value2", requestHeaders.get("key2"));
    }

    @Test
    public void removingSpacesGetRequestHeaders() {
        requestedLines.add("GET");
        requestedLines.add("key1 : value1");

        Request request = new Request(requestedLines);

        Map<String, String> requestHeaders = request.getRequestHeaders();

        assertEquals("value1", requestHeaders.get("key1"));
    }

    @Test
    public void multipleColonsGetRequestHeaders() {
        requestedLines.add("GET");
        requestedLines.add("key1:value1:value1");

        Request request = new Request(requestedLines);

        Map<String, String> requestHeaders = request.getRequestHeaders();

        assertEquals("value1:value1", requestHeaders.get("key1"));
    }
}