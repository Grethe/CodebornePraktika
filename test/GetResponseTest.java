import org.junit.Test;
import java.io.IOException;
import static java.util.Collections.singletonList;
import static org.junit.Assert.*;

public class GetResponseTest {

    @Test
    public void getResponse() throws IOException {
        Request request = new Request(singletonList("GET /test.txt"));
        Response response = new GetResponse(request);

        String statusCode = response.getStatusCode().toString();

        assertEquals(1, response.getHeaders().size());
        assertEquals("13", response.getHeaders().get("Content-Length").toString());
        assertArrayEquals(("rida 1\nrida 2").getBytes(), response.getBody());
        assertEquals("OK", statusCode);
    }

    @Test
    public void getFileNotFound() throws IOException {
        Request request = new Request(singletonList("GET /test.tx"));
        Response response = new GetResponse(request);

        String statusCode = response.getStatusCode().toString();

        assertEquals(0, response.getHeaders().size());
        assertArrayEquals(("").getBytes(), response.getBody());
        assertEquals("NOT_FOUND", statusCode);
    }
}