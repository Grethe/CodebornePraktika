import org.junit.Test;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import static java.util.Collections.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class ResponseTest {

    @Test
    public void responseToClient() throws IOException {

        Response response = spy(Response.class);
        doReturn(StatusCode.OK).when(response).getStatusCode();
        doReturn(singletonMap("key", "value")).when(response).getHeaders();
        doReturn("body".getBytes()).when(response).getBody();

        assertArrayEquals("HTTP/1.1 200 OK\nkey: value\n\nbody".getBytes(), response.responseToClient());
    }
}
