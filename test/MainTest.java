import org.junit.Test;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import static org.mockito.Mockito.*;

public class MainTest {

    @Test
    public void establishConnection() throws IOException {
        ServerSocket serverSocket = mock(ServerSocket.class);
        Socket clientSocket = mock(Socket.class);
        ClientHandler clientHandler = mock(ClientHandler.class);

        doReturn(clientSocket).when(serverSocket).accept();

        Main main = spy(Main.class);

        main.establishConnection(serverSocket, clientHandler);

        verify(clientHandler).handle(clientSocket);
    }
}