import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import static java.nio.charset.StandardCharsets.*;

public class Server {
    public static void main(String[] args) throws IOException {

        ServerSocket serverSocket = new ServerSocket(9090);

        while (true) {
            try (Socket clientSocket = serverSocket.accept();
                 BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                 DataOutputStream out = new DataOutputStream(clientSocket.getOutputStream())) {

                String line = in.readLine();
                System.out.println(line);

                if (line != null) {
                    String response = line.split(" ")[1];
                    String fileName = response.split("/")[1];

                    Path path = Paths.get("src", fileName);

                    if (path.toFile().exists()) {
                        byte[] dataFromFile = Files.readAllBytes(path);
                        String responseAndHeader = "HTTP/1.1 200 OK\nContent-Length: " + dataFromFile.length + "\nContent-type: " + Files.probeContentType(path) + "\n\n";
                        out.write(responseAndHeader.getBytes(UTF_8));

                        if ("GET".equals(line.split(" ")[0])){
                            out.write(dataFromFile);
                        }

                    } else {
                        out.write("HTTP/1.1 404 not found\n\n".getBytes());
                    }
                }

                out.flush();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
