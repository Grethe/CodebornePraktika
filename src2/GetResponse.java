import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Map;

public class GetResponse extends Response {
    private Path path;

    GetResponse(Request request) {
        path = Paths.get("src", request.getResource());
    }

    @Override
    public final Map<String, Object> getHeaders() {
        Map<String, Object> header = new LinkedHashMap<>();
        if (path.toFile().exists()) {
            header.put("Content-Length", path.toFile().length());
            return header;
        }
            return header;
    }

    @Override
    public byte[] getBody() throws IOException {
        if (path.toFile().exists()) {
            return Files.readAllBytes(path);
        }
        return new byte[0];
    }

    @Override
    public final StatusCode getStatusCode() {
        if (path.toFile().exists()) {
            return StatusCode.OK;
        }
        return StatusCode.NOT_FOUND;
    }
}

