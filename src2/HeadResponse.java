
public class HeadResponse extends GetResponse {
    public HeadResponse(Request request) {
        super(request);
    }

    @Override
    public byte[] getBody() {
        return new byte[0];
    }
}
