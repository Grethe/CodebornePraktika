import java.io.*;
import java.util.Map;

public abstract class Response {

    public abstract Map<String, Object> getHeaders();

    public abstract byte[] getBody() throws IOException;

    public abstract StatusCode getStatusCode();

    public byte[] responseToClient() throws IOException {

        String header = "";
        for (Map.Entry<String, Object> entry : getHeaders().entrySet()) {
            header += entry.getKey() + ": " + entry.getValue() + "\n";
        }

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        String statusLine = "HTTP/1.1 " + getStatusCode().getCode() + " " + getStatusCode().getStatusDescription();

        byte[] headerToBytes = header.getBytes();

        outputStream.write(statusLine.getBytes());
        outputStream.write("\n".getBytes());
        outputStream.write(headerToBytes);
        outputStream.write("\n".getBytes());
        outputStream.write(getBody());

        return outputStream.toByteArray();
    }
}
