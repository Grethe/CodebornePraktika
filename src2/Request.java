import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Request {
    private String method;
    private String resource;
    private Map<String, String> requestHeaders;
    char[] requestBody;

    Request(List<String> requestedLines) {

        String firstLine = requestedLines.get(0);

        requestHeaders = parseRequestHeaders(requestedLines);

        if (firstLine.contains(" ")) {
            method = firstLine.split(" ")[0];
            resource = firstLine.split(" ")[1];

        } else {
            resource = "not found";
        }
    }

    private Map<String, String> parseRequestHeaders(List<String> requestedLines) {

        requestedLines = requestedLines.subList(1, requestedLines.size());

        Map<String, String> requestHeaders = new LinkedHashMap<>();

        for(String line : requestedLines) {
            String[] splitLine = line.split(":", 2);
            requestHeaders.put(splitLine[0].trim(), splitLine[1].trim());
        }

        return requestHeaders;
    }

    public String getMethod() {
        return method;
    }

    public String getResource() {
        return resource;
    }

    public Map<String, String> getRequestHeaders() {
        return requestHeaders;
    }
}
