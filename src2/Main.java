import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Main {

    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(9090);

        Main main = new Main();

        while (true) {

            main.establishConnection(serverSocket, new ClientHandler());
            }
        }

    public void establishConnection(ServerSocket serverSocket, ClientHandler clientHandler) {
        try (Socket clientSocket = serverSocket.accept()) {

            clientHandler.handle(clientSocket);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


