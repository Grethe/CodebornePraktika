import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class ClientHandler {

    public void handle(Socket clientSocket) {
        try (BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
             OutputStream out = clientSocket.getOutputStream()) {

            List<String> requestLines = requestLines(in);

            if (!requestLines.isEmpty()) {

                Request request = bodyToRequest(requestLines, in);
                Response response = getResponse(request);

                out.write(response.responseToClient());
            }
            out.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<String> requestLines(BufferedReader in) throws IOException {

        List<String> requestedLines = new ArrayList<>();
        String line;

        while ((line = in.readLine()) != null && line.length() != 0) {
            requestedLines.add(line);
        }
        return requestedLines;
    }

    int getContentLength (List <String> requestedLines) {
        int contentLength = 0;
        for (String requestLine : requestedLines) {

            if (requestLine.contains("Content-Length")) {
                contentLength = Integer.parseInt(requestLine.split(":")[1].trim());
            }
        }
        return contentLength;
    }

    char[] requestBody(int contentLength, BufferedReader in) throws IOException {

        char[] body = new char[0];
        int x = 0;

        if (contentLength > 0) {

            body = new char[contentLength];

            while (x < contentLength) {
                int readChars = in.read(body, x, contentLength - x);

                if (readChars > -1) {
                    x += readChars;
                }
               else {
                    break;
                }
            }
        }
        return body;
    }

    Request bodyToRequest(List<String> requestedLines, BufferedReader in) throws IOException {
        Request request = new Request(requestedLines);

        int contentLength = getContentLength(requestedLines);

        request.requestBody = requestBody(contentLength, in);

        return request;
    }

    Response getResponse(Request request) {

        Response response;

        if (("GET").equals(request.getMethod())) {
            response = new GetResponse(request); }
        else if (("HEAD").equals(request.getMethod())) {
            response = new HeadResponse(request); }
        else {
            response = new BadRequestResponse();
        }
        return response;
    }
}
