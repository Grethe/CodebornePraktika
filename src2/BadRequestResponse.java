import java.util.Collections;
import java.util.Map;

public class BadRequestResponse extends Response {

    @Override
    public Map<String, Object> getHeaders() {
        return Collections.emptyMap();
    }

    @Override
    public byte[] getBody() {
        return new byte[0];
    }

    @Override
    public StatusCode getStatusCode() {
        return StatusCode.BAD_REQUEST;
    }
}
