 public enum StatusCode {

    OK(200, "OK"), BAD_REQUEST(400, "bad request"), NOT_FOUND(404, "not found");

    private int code;
    private String statusDescription;

    StatusCode(int code, String statusDescription) {
        this.code = code;
        this.statusDescription = statusDescription;
    }

     public int getCode() {
         return code;
     }

     public String getStatusDescription() {
         return statusDescription;
     }
 }
